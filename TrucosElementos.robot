*** Settings ***
Library      SeleniumLibrary
Resource     Recursos.robot

*** Test Cases ***
C001 Primer Caso Para Abrir un Hinpervinculo
    Open Homepage
    Title Should Be     Hola Mundo!
    Click Element       xpath=/html/body/div[1]/div/div[2]/a[1]
    Wait Until Element Is Visible    xpath=//*[@id="post-451"]/div[1]/img
    Close Browser
C002 Segunda Caso Para Abrir un Hinpervinculo
    Open Homepage
    Title Should Be          Hola Mundo!
    Click Element            xpath=/html/body/div[1]/div/div[2]/a[2]
    Wait Until Element Is Visible   xpath=//*[@id="exampleModal"]/div/div/div[3]/button[1]
    Set Focus To Element     xpath=//*[@id="exampleModal"]/div/div/div[3]/button[1]
    Click Button             xpath=//*[@id="exampleModal"]/div/div/div[3]/button[1]
    Close Browser

